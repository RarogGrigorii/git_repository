﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawn : MonoBehaviour
{
    public float spawnDelay = 5.9f;
    public Enemy enemyPrefab;

    public NavMeshAgent navMeshAgent;
    public Transform target;

    public Transform[] wayPoint;

    private int index = 0;

    private void MoveToWayPoint()
    {
        target = wayPoint[index];
    }

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemy", 0.0f, spawnDelay);
    }

    // Update is called once per frame
    void Update()
    {
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        if ((transform.position - playerTransform.position).magnitude < 20)
        {
            navMeshAgent.SetDestination(playerTransform.position);
        }
        
        else
            {navMeshAgent.SetDestination(target.position);

            if ((transform.position - target.position).magnitude < 0.3f)
            {
                index++;
                if (index >= wayPoint.Length)
                {
                    index = 0;
                }
            MoveToWayPoint();
        }
        }
    }

    public void SpawnEnemy()
    {
        Enemy newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        newEnemy.Target = GameObject.FindGameObjectWithTag("Player").transform;
    }
}
