﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    public float damage = 10f;
    public float Damage
	{         
        get { return damage; }         
        set { damage = value; }
	}
    private void OnCollisionEnter(Collision collision)
    {
        Destructable target = collision.gameObject.GetComponent<Destructable>();

        if (target != null)
	    {
    	    target.Hit(Damage);
	    }

        Destroy(gameObject);
    }

    void Start()
    {
        
    }
}
