﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField]
    private float spawnDelay;

    [SerializeField]
    private Enemy enemyPrefab;

    public void Spawn()
    {
    	Enemy newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity) as Enemy;

        newEnemy.Target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Start()
    {
        InvokeRepeating("Spawn", 0.0f, spawnDelay);
    }

    void Update()
    {
        
    }
}
