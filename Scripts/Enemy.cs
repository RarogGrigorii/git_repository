﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;

    [SerializeField]
	private GameObject bulletPrefab;

    [SerializeField]
	private Transform EnemyGun;

    [SerializeField]
    private float shootPower;

    [SerializeField]
    private float shootDelay;

    [SerializeField]
    private float bulletDamage = 10f;

    private bool seeTarget;

    [SerializeField]
    private Transform target;

    public Transform Target { get => target; set => target = value; }

    void Awake()
    {
        _navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        InvokeRepeating("Shoot", 1.0f, shootDelay);
    }
    void Update()
    {
        CheckTargetVisibility(); 

        _navMeshAgent.SetDestination(Target.position);

    }
    private void Shoot()
    {
        if (seeTarget == true)
        {
            GameObject newBullet = Instantiate(bulletPrefab, EnemyGun.position, EnemyGun.rotation) as GameObject;
            newBullet.GetComponent<Rigidbody>().AddForce(EnemyGun.forward * shootPower);
            Destroy(newBullet, 5);
            newBullet.GetComponent<Damager>().Damage = bulletDamage;
        }
    }
    private void CheckTargetVisibility()
    {
        Vector3 targetDirection = target.position - EnemyGun.transform.position;

        targetDirection.Normalize();
 
        Ray ray = new Ray(EnemyGun.transform.position, targetDirection);

        RaycastHit hit;
  
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == target)
            {
                seeTarget = true;
                return;
            }
        }

		seeTarget = false;
    }
}
